import java.util.Arrays;

public class lab23 {
    public static int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0; // Empty array, no duplicates to remove
        }

        int k = 1; // Initialize k to count unique elements (start from index 1)
        
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[i - 1]) {
                nums[k] = nums[i]; // Move unique elements to the front
                k++;
            }
        }
        
        return k; // k represents the number of unique elements
    }

    public static void main(String[] args) {
        int[] nums1 = {1, 1, 2};
        int result1 = removeDuplicates(nums1);
        System.out.println("Output for Example 1: " + result1 + ", nums = " + Arrays.toString(nums1));

        int[] nums2 = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        int result2 = removeDuplicates(nums2);
        System.out.println("Output for Example 2: " + result2 + ", nums = " + Arrays.toString(nums2));
    }
}
