import java.util.Arrays;
public class lab21 {
   public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index < 0 || index >= arr.length) {
            System.out.println("Invalid index. Element not deleted.");
            return arr;
        }

        int[] updatedArray = new int[arr.length - 1];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (i != index) {
                updatedArray[j] = arr[i];
                j++;
            }
        }

        return updatedArray;
    }

    public static int[] deleteElementByValue(int[] arr, int value) {
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            System.out.println("Value not found in the array. Element not deleted.");
            return arr;
        }

        int[] updatedArray = new int[arr.length - 1];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (i != index) {
                updatedArray[j] = arr[i];
                j++;
            }
        }

        return updatedArray;
    }

    public static void main(String[] args) {
        int[] originalArray = {1, 2, 3, 4, 5};

        System.out.println("Original Array: " + Arrays.toString(originalArray));

        // Test deleteElementByIndex method
        int indexToDelete = 2;
        int[] arrayAfterIndexDeletion = deleteElementByIndex(originalArray, indexToDelete);
        System.out.println("Array after deleting element at index " + indexToDelete + ": " + Arrays.toString(arrayAfterIndexDeletion));

        // Test deleteElementByValue method
        int valueToDelete = 4;
        int[] arrayAfterValueDeletion = deleteElementByValue(arrayAfterIndexDeletion, valueToDelete);
        System.out.println("Array after deleting element with value " + valueToDelete + ": " + Arrays.toString(arrayAfterValueDeletion));
    }
}
